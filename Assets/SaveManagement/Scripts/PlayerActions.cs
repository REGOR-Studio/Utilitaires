using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour
{
    private void OnPause()
    {
        if (LevelManager.Instance.paused)
            LevelManager.Instance.UnPause();
        else
            LevelManager.Instance.Pause();
    }
}